package org.dstorerdb.logger;

import org.dstorerdb.logger.time.Calendar;
import org.dstorerdb.logger.time.Clock;

public class Time {

    public static String getCurrentTime() {
        StringBuilder sbTime = new StringBuilder();
        Calendar calendar = new Calendar();
        Clock clock = new Clock();

        byte h = clock.getHours();
        byte m = clock.getMinutes();
        byte s = clock.getSeconds();

        sbTime.append(calendar.getMonth()).append(' ')
                .append(calendar.getMonthDay()).append(", ")
                .append(calendar.getYear()).append(' ')
                .append(h < 10 ? "0"+h : h).append(':')
                .append(m < 10 ? "0"+m : m).append(':')
                .append(s < 10 ? "0"+s : s);

        return sbTime.toString();
    }

    public static void main(String[] args) {
        System.out.println(Time.getCurrentTime());
    }

}
