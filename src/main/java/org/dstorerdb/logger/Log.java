package org.dstorerdb.logger;

import java.io.IOException;

import static org.dstorerdb.logger.Log.LogType.*;

public class Log {
    public static Log getLog(Class clazz) {
        return new Log(clazz.getName());
    }

    public static Log getLog(Object obj) {
        return new Log(obj);
    }

    public static Log getLog(String className) {
        return new Log(className);
    }

    public static enum LogType {
        INFO, WARNING, ERROR, RAW
    }

    private static final String INFO_COLOR = ConsoleColor.ANSI_GREEN;
    private static final String WARNING_COLOR = ConsoleColor.ANSI_YELLOW;
    private static final String ERROR_COLOR = ConsoleColor.ANSI_RED;
    private static final String RAW_COLOR = ConsoleColor.ANSI_RESET;

    private final String className;

    public Log(Class clazz) {
        if (clazz == null)
            this.className = "null";
        else
            this.className = clazz.getName();
    }

    public Log(Object obj) {
        if (obj == null)
            this.className = "null";
        else
            this.className = obj.getClass().getName();
    }

    public Log(String className) {
        this.className = className;
    }

    private String getMessageColor(LogType type) {
        switch (type) {
            case INFO:
                return INFO_COLOR;

            case ERROR:
                return ERROR_COLOR;

            case WARNING:
                return WARNING_COLOR;

            default:
                return RAW_COLOR;
        }
    }

    private void appendMsg(StringBuilder sbMsg, String[] msgSplit, LogType logType) {
        String toAppend;
        switch (logType) {
            case INFO:
                toAppend = "info\n---- INFO: ";
                break;

            case ERROR:
                toAppend = "error\n---- ERROR: ";
                break;

            case WARNING:
                toAppend = "warning\n---- WARN: ";
                break;

            default:
                toAppend = "raw\n---- ";
                break;
        }
        String[] toAppSplit = toAppend.split("\n");
        sbMsg.append(toAppSplit[0]).append('\n');
        toAppend = toAppSplit[1];

        for (int i = 0; i < msgSplit.length; i++)
            sbMsg.append(toAppend).append(msgSplit[i]).append('\n');
    }

    private String getMsg(String msg, LogType logType) {
        StringBuilder sbMsg = new StringBuilder();
        sbMsg.append(ConsoleColor.ANSI_RED);
        sbMsg.append('[');
        sbMsg.append(Time.getCurrentTime())
                .append("] ")
                .append(ConsoleColor.RESET)
                .append(className).append(' ')
                .append(getMessageColor(logType));
        appendMsg(sbMsg, msg.split("\n"), logType);
        sbMsg.append(ConsoleColor.RESET);
        return sbMsg.toString();
    }

    private void println(String str) {
        try {
            SystemUtil.getStdout().write(str.concat("\n").getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void print(String str) {
        try {
            SystemUtil.getStdout().write(str.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void error(String msg) {
        println(getMsg(msg, ERROR));
    }

    public void info(String msg) {
        println(getMsg(msg, INFO));
    }

    public void warning(String msg) {
        println(getMsg(msg, WARNING));
    }

    public void raw(String msg) {
        println(getMsg(msg, RAW));
    }

    /*public void rawError() {
        println(getRawMsg(ERROR_COLOR));
    }

    public void rawInfo() {
        println(getRawMsg(INFO_COLOR));
    }

    public void rawWarning() {
        println(getRawMsg(WARNING_COLOR));
    }

    public void rawMessage() {
        println(getRawMsg(RAW_COLOR));
    }*/

    public static void main(String[] args) {
        Log.getLog(Log.class).warning("linea1\nlinea2");
    }

}
