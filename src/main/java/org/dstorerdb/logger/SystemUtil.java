package org.dstorerdb.logger;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class SystemUtil {

    public static FileInputStream getStdin() {
        return new FileInputStream(FileDescriptor.in);
    }

    public static FileOutputStream getStdout() {
        return new FileOutputStream(FileDescriptor.out);
    }

}
