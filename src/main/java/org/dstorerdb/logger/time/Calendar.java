package org.dstorerdb.logger.time;

import java.util.GregorianCalendar;

public class Calendar {

    private java.util.Calendar cal;

    public Calendar() {
        cal = new GregorianCalendar();
    }

    public byte getMonthDay() {
        return (byte) cal.get(java.util.Calendar.DAY_OF_MONTH);
    }

    public String getMonth() {
        byte month = (byte) (cal.get(java.util.Calendar.MONTH)+1);
        switch (month) {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "Mar";
            case 4:
                return "Apr";
            case 5:
                return "May";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case 8:
                return "Aug";
            case 9:
                return "Sep";
            case 10:
                return "Oct";
            case 11:
                return "Nov";
            default:
                return "Dec";
        }
    }

    public short getYear() {
        return (short) cal.get(java.util.Calendar.YEAR);
    }

}
