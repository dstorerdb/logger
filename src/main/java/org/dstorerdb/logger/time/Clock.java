package org.dstorerdb.logger.time;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Clock {

    private Calendar cal;
    
    public Clock() {
        cal = new GregorianCalendar();
    }

    public byte getHours() {
        return (byte) cal.get(Calendar.HOUR_OF_DAY);
    }

    public byte getMinutes() {
        return (byte) cal.get(Calendar.MINUTE);
    }

    public byte getSeconds() {
        return (byte) cal.get(Calendar.SECOND);
    }

}
