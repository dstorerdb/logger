- 0.1 RC
	- Se suben clases iniciales del proyecto.

- 0.2 RC
	- Se añade clase SystemUtil para imprimir mensajes directamente desde el descriptor de archivos stdout.

- 0.3 RC
    - Se cambia es color definido para info.
    - Se optimiza un poco clase Logger.

- 0.4 RC
    - Se añade funcionalidad para imprimir mesnajes en color blanco.
    
- 0.5-RC
    - Se simplifica el logger adaptandolo a lo que se necesita
    
- 0.6-RC
    - Se agrega soporte multilinea
